const https = require('https')
const StreamArray = require('stream-json/streamers/StreamArray');
const config = require('./config');

const db = require('./models');

const insertGames = async (gamesList, platform) => {
  gamesList.forEach(game => {
    const mappedGame = {
      name: game.name,
      publisherId: game.publisher_id,
      platform,
      bundleId: game.bundle_id,
      appVersion: game.version,
      isPublished: true,
    }
    db.Game.create(mappedGame)
  })
}

const importFile = async (source) => {
  https.get(source.url, response => {
    let pipeline

    pipeline = response
      .pipe(StreamArray.withParser())

    pipeline.on('data', async (data) => insertGames(data.value, source.platform))

  })
}

const importTopGames = async () => {
  const dataSources = config.topDataSources
  dataSources.forEach((source) => importFile(source))
}


module.exports = {
  importTopGames
}