module.exports = {
  platforms: ['ios', 'android'],
  topDataSources: [
    {
      platform: 'android',
      url: 'https://interview-marketing-eng-dev.s3.eu-west-1.amazonaws.com/android.top100.json',
    },
    {
      platform: 'ios',
      url: 'https://interview-marketing-eng-dev.s3.eu-west-1.amazonaws.com/ios.top100.json',
    }
  ]
}